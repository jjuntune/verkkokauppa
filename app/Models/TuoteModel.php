<?php  namespace App\Models;

use CodeIgniter\Model;

class TuoteModel extends Model {
  protected $table = 'tuote';

  protected $allowedFields = ['nimi'];

  public function haeTuoteryhmalla($tuoteryhma_id) {
    $builder = $this->table('tuote');
    return $builder->getWhere(['tuoteryhma_id' => $tuoteryhma_id])->getResultArray();
  }

  public function haeTuotteet($idt) {
    $palautus = array();
    foreach ($idt as $id) {
      $this->table('tuote');
      $this->select('id,nimi,hinta');
      $this->where('id',$id);
      $query = $this->get();
      $tuote = $query->getRowArray();
      $this->lisaaTuoteTaulukkoon($tuote,$palautus);
        
      $this->resetQuery();
    }
   
    return $palautus;
  }

  public function haeTuote($id) {
    $this->where('id',$id);
    $query = $this->get();
    $tuote = $query->getRowArray();
    // Voidaan käyttää debuggauksessa, kun halutaan tietää, mikä
    // kysely suoritettiin.
    //echo $this->getLastQuery(); 
    return $tuote;
  }

  // Lisää tuotteen taulukkoon tai päivittää määrää, jos tuote on jo taulukossa.
  private function lisaaTuoteTaulukkoon($tuote,&$taulukko) {
    for ($i = 0;$i < count($taulukko);$i++ ) {
      if ($taulukko[$i]['id'] === $tuote['id']) {
        $taulukko[$i]['maara'] = $taulukko[$i]['maara']  + 1;
        return;
      }
    }
    $tuote['maara'] = 1; // Tuote ei ollut taulukossa, joten asetetaan määräksi 1.
    array_push($taulukko,$tuote);
  }
}