<?php  namespace App\Models;

use CodeIgniter\Model;

class TuoteryhmaModel extends Model {
  protected $table = 'tuoteryhma';

  protected $allowedFields = ['nimi'];

  public function haeKaikki() {
    return $this->findAll();
  }

  public function hae($tuoteryhma_id) {
    $this->where('id',$tuoteryhma_id);
    $query = $this->get();
    return $query->getRowArray();
  }

  public function haeEnsimmainenTuoteryhma() {
    $this->select('id');
    $this->orderBy('id','asc');
    $this->limit(1);
    $query = $this->get();
    $tuoteryhma = $query->getRowArray();
    return $tuoteryhma['id'];
  }


  public function poista($id) {
    try {
      $this->where('id',$id);
      $this->delete();
    }
    catch (\Exception $ex) {
      throw new \Exception($ex->getMessage(),$ex->getCode(),$ex);
    }
  }
}