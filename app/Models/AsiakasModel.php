<?php  namespace App\Models;

use CodeIgniter\Model;

class AsiakasModel extends Model {
  protected $table = 'asiakas';

  protected $allowedFields = ['etunimi','sukunimi','lahiosoite','postinumero','postitoimipaikka','email','puhelin'];
}