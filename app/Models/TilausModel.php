<?php  namespace App\Models;

use CodeIgniter\Model;

use App\Models\AsiakasModel;
use App\Models\TuoteriviModel;

class TilausModel extends Model {
  protected $table = 'tilaus';

  protected $allowedFields = ['tila','asiakas_id'];
  
  public function tallenna($asiakas, $tuote_idt) {
    $this->db->transStart();

    $asiakas_id = $this->tallennaAsiakas($asiakas);
    
    $tilaus_id = $this->tallennaTilaus($asiakas_id);
   
    $this->tallennaTilausrivit($tilaus_id,$tuote_idt);
    $this->db->transComplete();

    /*
    if ($this->db->transStatus() === FALSE)
    {
    
    }
    */
  }

  private function tallennaAsiakas($asiakas) {
    $asiakasModel= new AsiakasModel();
    $asiakasModel->save($asiakas);
    return $this->insertID();
  }

  private function tallennaTilaus($asiakas_id) {
    $this->save([
      'tila' => 'tilattu',
      'asiakas_id' => $asiakas_id
    ]);
    return $this->insertID();
  }

  private function tallennaTilausrivit($tilaus_id,$tuote_idt) {
    $tuoteriviModel = new TuoteriviModel();
    foreach ($tuote_idt as $tuote_id) {
      $tuoteriviModel->save([
        'tilaus_id' => $tilaus_id,
        'tuote_id' => $tuote_id,
        'maara' => 1
      ]);
    }
  }
 
}