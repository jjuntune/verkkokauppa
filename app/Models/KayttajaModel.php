<?php  namespace App\Models;

use CodeIgniter\Model;

class KayttajaModel extends Model {
  protected $table = 'kayttaja';

  protected $allowedFields = ['tunnus','salasana','kuva'];

  public function tarkasta($tunnus,$salasana) {
    $this->where('tunnus',  $tunnus);
    $query = $this->get();
    $tietue = $query->getRow();
    if ($tietue) { 
      if (password_verify($salasana,$tietue->salasana)) {
        return $tietue;
      }
    }
    return null; 
  }
}