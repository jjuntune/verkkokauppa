<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Kauppa::index');
$routes->get('kauppa/tuote/(:segment)', 'Kauppa::tuote/$1');
$routes->get('kauppa/yhteystiedot', 'Kauppa::yhteystiedot');
$routes->get('kauppa/toimitusehdot', 'Kauppa::toimitusehdot');
$routes->get('kauppa/(:segment)', 'Kauppa::index/$1');
$routes->get('ostoskori/lisaa/(:segment)', 'Ostoskori::lisaa/$1');
$routes->get('ostoskori/poista/(:segment)', 'Ostoskori::poista/$1');
$routes->get('ostoskori', 'Ostoskori::index');
$routes->get('tyhjenna', 'Ostoskori::tyhjenna');
$routes->get('tilaus/tilaa', 'Tilaus::tilaa');
$routes->get('yllapito', 'Yllapito::index');
$routes->get('kayttaja/index','Kayttaja::index');
$routes->get('kayttaja/kirjaudu','Kayttaja::kirjaudu');
$routes->get('kayttaja/rekisteroityminen','Kayttaja::rekisteroityminen');
$routes->get('kayttaja/naytaRekisteroitymisLomake','Kayttaja::naytaRekisteroitymisLomake');
$routes->get('tuoteryhma/tallenna', 'Tuoteryhma::tallenna');
$routes->get('tuoteryhma/poista/(:segment)', 'Tuoteryhma::poista/$1');
$routes->get('tuoteryhma', 'Tuoteryhma::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
