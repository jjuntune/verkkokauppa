<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;

class Tuoteryhma extends BaseController
{

	private $tuoteryhmaModel = null;

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		$this->tuoteryhmaModel = new TuoteRyhmaModel();
	}

	public function index($tuoteryhma_id=null)
	{
		if (!isset($_SESSION['kayttaja'])) {
      return redirect('kayttaja/kirjaudu');
		}

		$data['error'] = '';
		if (isset($_SESSION['error'])) {
			$data['error'] = $_SESSION['error'];
		}
	
    $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeKaikki();
		echo view('admin/templates/admin_header');
		echo view('admin/tuoteryhma_view',$data);
		echo view('admin/templates/admin_footer');
  }
  
  public function tallenna($tuoteryhma_id = null) {
		if (!isset($_SESSION['kayttaja'])) {
      return redirect('kayttaja/kirjaudu');
    }

		$data['error']=null;
		// Jos get metodi, näytetään suoraan lomake ilman validointeja.
		if ($this->request->getMethod() === 'get') {
			$data['id'] = '';
			$data['nimi'] = '';
			if ($tuoteryhma_id != null) {
				$tuoteryhma = $this->tuoteryhmaModel->hae($tuoteryhma_id);
				$data['id'] = $tuoteryhma['id'];
				$data['nimi'] = $tuoteryhma['nimi'];
			}
			$this->nayta($data);
		}
		else {
			// Tarkastetaan syötteet.
			if (!$this->validate([
				'nimi' => 'required|max_length[255]',
			])) {
			// Jos syötteet ei kunnossa, palataan lomakkeelle.
				$this->nayta($data);
			}
			else {
			// Syötteet kunnossa, tallennetaan.

				$tuoteryhma_id = $this->request->getPost('id');

				try {
					$this->tuoteryhmaModel->save([
						'id' => $tuoteryhma_id,
						'nimi' => $this->request->getPost('nimi')
					]);
					return redirect('tuoteryhma');
				}
				// Tallennuksessa tapahtui virhe, palataan lomakkeelle ja näytetään virhe.
				catch (\Exception $ex) {
					$data['error'] = $ex->getMessage();
					$this->naytaLomake($data);
				}
			}
		}
  }

  public function poista($id) {
		// Poiston saa suorittaa vain ylläpitääjä.
		if (!isset($_SESSION['kayttaja'])) {
      return redirect('kayttaja/kirjaudu');
		}
		
		// Tarkastetaan, että id on numeerinen (eikä esim. sql-injektio).   
		if (!is_numeric($id)) {
			throw new \Exception('Id ei ole numero.');
		}
		
		try {
			$this->tuoteryhmaModel->poista($id);
		}
		catch (\Exception $ex) {
			$_SESSION['error'] = $ex->getMessage();
			$session = session();
			$session->markAsFlashdata('error');
		}
		return redirect('tuoteryhma');
	}
	
	private function nayta($data) {
		echo view('admin/templates/admin_header');
		echo view('admin/tuoteryhma_muokkaa_view',$data);
		echo view('admin/templates/admin_footer');
	}
}
