<?php namespace App\Controllers;


use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;

class Kauppa extends BaseController
{

	private $tuoteryhmaModel = null;
	private $tuoteModel = null;

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		$this->tuoteryhmaModel = new TuoteRyhmaModel();
		$this->tuoteModel = new TuoteModel();
	}

	public function index($tuoteryhma_id=null)
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeKaikki();
		// hae eka tuoteryhma jos null
		if ($tuoteryhma_id === null) {
			$tuoteryhma_id = $this->tuoteryhmaModel->haeEnsimmainenTuoteryhma();
		}
		$data['tuoteryhma_id'] = $tuoteryhma_id;
		$_SESSION['tuoteryhma_id'] = $tuoteryhma_id;
		// Hae tuotteet tuoteryhman perusteella
		$data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
		echo view('templates/header');
		echo view('kauppa_view',$data);
		echo view('templates/footer');
	}

	public function tuote($tuote_id) {
		$data['tuote'] = $this->tuoteModel->haeTuote($tuote_id);
		echo view('templates/header');
		echo view('tuote_view',$data);
		echo view('templates/footer');
	}

	public function yhteystiedot() {
		echo view('templates/header');
		echo view('yhteystiedot_view');
		echo view('templates/footer');
	}

	public function toimitusehdot() {
		echo view('templates/header');
		echo view('toimitusehdot_view');
		echo view('templates/footer');
	}
}
