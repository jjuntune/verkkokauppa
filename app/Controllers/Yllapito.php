<?php namespace App\Controllers;


use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;

class Yllapito extends BaseController
{

	private $tuoteryhmaModel = null;
	private $tuoteModel = null;

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		$this->tuoteryhmaModel = new TuoteRyhmaModel();
		$this->tuoteModel = new TuoteModel();
	}

	public function index($tuoteryhma_id=null)
	{		
    $data = [];
		echo view('admin/templates/admin_header' , ['title' => 'Verkkokauppa ylläpito']);
		echo view('admin/yllapito_view',$data);
		echo view('admin/templates/admin_footer');
	}
}
