<?php namespace App\Controllers;

use App\Models\TilausModel;

class Tilaus extends BaseController
{

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
	}

	public function tilaa()
	{
    $tilausModel = new TilausModel();

    $asiakas = [
      'etunimi' => $this->request->getPost('etunimi'),
      'sukunimi' => $this->request->getPost('sukunimi'),
      'lahiosoite' => $this->request->getPost('lahiosoite'),
      'postinumero' => $this->request->getPost('postinumero'),
      'postitoimipaikka' => $this->request->getPost('postitoimipaikka'),
      'email' => $this->request->getPost('email'),
      'puhelin' => $this->request->getPost('puhelin')
    ];

    $tilausModel->tallenna($asiakas, $_SESSION['kori']);
    unset($_SESSION['kori']);
    echo view('templates/header' );
		echo view('kiitos_view');
		echo view('templates/footer');
	}
}
