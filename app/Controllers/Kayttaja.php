<?php namespace App\Controllers;

use App\Models\KayttajaModel;

class Kayttaja extends BaseController
{

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		$this->kayttajaModel = new KayttajaModel();
	}

  public function index() {
    echo view('admin/templates/admin_header' );
		echo view('admin/profiili_view');
    echo view('admin/templates/admin_footer');
  }

  public function kirjaudu() {
    
    if (!$this->validate([ 
        'tunnus' => 'required|min_length[8]|max_length[30]',
        'salasana' => 'required|min_length[8]|max_length[30]',
    ])){
      echo view('admin/templates/admin_header');
      echo view('admin/kirjaudu_view');
      echo view('admin/templates/admin_footer');
    }
    else {
        // Tarkastetaan tunnus ja salasana.
        $kayttaja = $this->kayttajaModel->tarkasta( 
          $this->request->getVar('tunnus'),
          $this->request->getVar('salasana')
        );
        // Tunnus ja salasana oikein, ohjataan käyttäjä yllapitoon.
        if ($kayttaja) { 
          $_SESSION['kayttaja'] = $kayttaja;
          return redirect('yllapito');  
        }
        // Käyttäjää ei löydy annetulla tunnuksella ja salasanalla, ohjataan takaisin kirjautumiseen.
        else { 
          return redirect('kayttaja/kirjaudu'); 
        }
    }
  }

  public function rekisteroityminen()
	{
    // Jos lomake avataan, näytetään rekisteröitymislomake (eikä validointia suoriteta).
    if ($this->request->getMethod() !== 'post') {
      return redirect('kayttaja/naytaRekisteroitymisLomake');
    }

    if (!$this->validate([
      'kuva' => [
        'uploaded[kuva]',
        'mime_in[kuva, image/jpg,image/jpeg,image/gif,image/png]',
        'max_size[kuva,4096]'
      ]
    ])) {
      return redirect('kayttaja/naytaRekisteroitymisLomake')->withInput();
    }
    else {
      $kuva = $this->request->getFile('kuva');
      $kuva->move($this->getUploadsPath());

      $this->kayttajaModel->save([ 
        'tunnus' => $this->request->getVar('tunnus'),
        'salasana' => password_hash($this->request->getVar('salasana'),PASSWORD_DEFAULT),
        'kuva' => $kuva->getName()
      ]);
      return redirect('kayttaja/kirjaudu');
    }
  }

  public function naytaRekisteroitymisLomake() {
    $tunnus = $this->request->getVar('tunnus');
    $data['tunnus'] = $tunnus;
    echo view('admin/templates/admin_header');
    echo view('admin/rekisteroityminen_view',$data);
    echo view('admin/templates/admin_footer');
  }
  
  private function getUploadsPath() {
    $uploadsPath = APPPATH;
    $uploadsPath = substr($uploadsPath, 0, -1);
    $uploadsPath = substr($uploadsPath, 0, strrpos( $uploadsPath, '/')) .'/public/uploads';
    return $uploadsPath;
  }
}
