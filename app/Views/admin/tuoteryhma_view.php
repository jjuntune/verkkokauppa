<h4>Tuoteryhmät</h4>
<div class="yllapito_lisaa">
  <a class="yllapito_poista" href="<?=site_url('tuoteryhma/tallenna')?>">
    <i class="fas fa-plus-circle fa-lg"></i>
  </a>
</div>
<div>
  <?= $error?>
  </div>
<table class="table">
<?php foreach ($tuoteryhmat as $tuoteryhma): ?>
  <tr>
    <td><?=$tuoteryhma['nimi']?></td>
    <td>
      <a class="yllapito_tallenna" href="<?=site_url('tuoteryhma/tallenna/' . $tuoteryhma['id'])?>">
        <i class="fas fa-edit"></i>
      </a>
    </td>
    <td>
      <a class="yllapito_poista" href="<?=site_url('tuoteryhma/poista/' . $tuoteryhma['id'])?>">
        <i class="fas fa-minus-circle"></i>
      </a>
    </td>
  </tr>
<?php endforeach;?>
</table>
