<form method="post" action="<?= site_url('kayttaja/rekisteroityminen')?>" enctype="multipart/form-data">
<div>
<?= \Config\Services::validation()->listErrors();?>
</div>
<div class="form-group">
  <label><?= lang('Kayttaja.tunnus');?></label>
  <input name="tunnus" class="form-control" maxlength="100" value="<?= old('tunnus')?>">
</div>
<div class="form-group">
  <label>Salasana</label>
  <input name="salasana" class="form-control" maxlength="100" value="<?= old('salasana')?>">
</div>
<div class="form-group">
  <label>Profiilikuva</label>
  <input type="file" name="kuva" class="form-control">
</div>
<button class="btn btn-primary">Save</button>
</form>