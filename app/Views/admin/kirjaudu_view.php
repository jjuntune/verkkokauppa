<form method="post" action="<?= site_url('kayttaja/kirjaudu')?>">
<div>
<?= \Config\Services::validation()->listErrors();?>
</div>
<div class="form-group">
  <label>Tunnus</label>
  <input name="tunnus" class="form-control" maxlength="100">
</div>
<div class="form-group">
  <label>Salasana</label>
  <input name="salasana" type="password" class="form-control" maxlength="100">
</div>
<button class="btn btn-primary">Kirjaudu</button>
</form>