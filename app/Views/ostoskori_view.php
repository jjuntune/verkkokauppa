<div class="row">  
  <div class="col">
    <h4>Ostoskori</h4>
    <?php
    $summa = 0;
    ?>
    <table class="table">
    <?php foreach ($tuotteet as $tuote): ?>
      <tr>
        <td>
          <?= $tuote['nimi']?>
        </td>  
        <td>
          <?= $tuote['hinta'] . ' €'?>
        </td>
        <td>
           <?= $tuote['maara'] ?> 
        </td>
        <td>
          <a class="ostoskori_poista"href="<?= site_url('ostoskori/poista/' . $tuote['id'])?>">
            <i class="fas fa-minus-circle"></i>
          </a>
        </td>
      </tr>
      <?php
      $summa += $tuote['hinta'] * $tuote['maara'];
      ?>
    <?php endforeach;?>
    <tr>
      <td></td>
      <td><? printf("%.2f €",$summa);?></td>
      <td></td>
      <td>   
        <a id="tyhjenna" href="<?= site_url('ostoskori/tyhjenna');?>">
          <i class="fas fa-trash"></i>
        </a>
      </td>
    </tr>
    </table>
  
    <h4>Tilaajan tiedot</h4>
    <form action="<?= site_url('tilaus/tilaa')?>" method="post">
      <div class="form-group">
        <label>Etunimi</label>
        <input name="etunimi" maxlength="50" class="form-control">
      </div>
      <div class="form-group">
        <label>Sukunimi</label>
        <input name="sukunimi" maxlength="100" class="form-control">
      </div>
      <div class="form-group">
        <label>Lähiosoite</label>
        <input name="lahiosoite" maxlength="100" class="form-control">
      </div>
      <div class="form-group">
        <label>Postinumero</label>
        <input name="postinumero" maxlength="5" class="form-control">
      </div>
      <div class="form-group">
        <label>Postitoimipaikka</label>
        <input name="postitoimipaikka" maxlength="100" class="form-control">
      </div>
      <div class="form-group">
        <label>Sähköposti</label>
        <input name="email" type="email" maxlength="255" class="form-control">
      </div>
      <div class="form-group">
        <label>Puhelin</label>
        <input name="puhelin" maxlength="20" class="form-control">
      </div>
      <button class="btn btn-primary">Tilaa</button>
    </form>
  </div>
</div>