<div class="row">  
  <div class="col-lg-3">
  <ul class="tuoteryhma">
  <?php foreach ($tuoteryhmat as $tuoteryhma): ?>
  <li
  <?php
  if ($tuoteryhma['id'] === $tuoteryhma_id) {
    echo "style='font-weight: bold'";
  } 
  ?>>
  <a href="<?= site_url('kauppa/' . $tuoteryhma['id']);?>"><?= $tuoteryhma['nimi'] ?></a>
  </li>
  <?php endforeach;?>
  </ul>
  </div>
  <div class="col-lg-9">
    <div class="container">
      <div class="row">
        <?php foreach ($tuotteet as $tuote): ?> 
          <div class="card">
            <a href="<?= site_url('kauppa/tuote/' . $tuote['id'])?>">
            <img class="card-img-top" src="<?= base_url('img/' . $tuote['kuva'])?>">
            </a>
            <div class="card-body>">
              <h4><?= $tuote['nimi'];?></h4>
              <p><?= $tuote['kuvaus'];?></p>
              <p class="hinta"><?= $tuote['hinta'];?> €</p>
            </div>
          </div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>